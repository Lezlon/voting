# 🗳️ Système de vote - Contrat Intelligent

Par Nathan Hallez\
Groupe N

## 📎 Présentation

Ce projet contient un contrat `Voting.sol` et une dApp React pour intéragir avec le contrat.

Le contrat est un système de vote qui permet à un utilisateur de créer un vote de manière décentralisé dans la blockchain.
Quand vous lancez le projet avec Hardhat, l'adresse 0 est l'adresse qui est l'administrateur.

Vidéo de présentation: https://www.youtube.com/watch?v=_0a5r7YWRjk

## ❓ Où se trouve le contrat?

Le contrat est situé dans le dossier `packages/backend/contracts`

## ➕ Fonctionnalités additionnelles

Pour cette application j'ai ajouté un système de délégation:
Un électeur qui ne sait pas pour quoi voter peut déléguer son vote à quelqu'un d'autre, son vote comptera alors comme deux.

J'ai aussi fait en sorte qu'on puisse voir combien de votes ont été effectués lors de la session de votes. Cela permet à l'administrateur de savoir où en sont les votes avant de passer à l'étape suivante.

## ⚙️ Installation

1.  Télécharger le projet et installer les dépendances

```bash
git clone https://gitlab.com/Lezlon/voting.git
cd voting
npm i
```

2.  Une fois que l'installation est terminée, lancez la commande `npm run chain` pour démarrer l'environnement hardhat.
3.  Ouvrez un autre terminal et ouvrez le dossier du projet
4.  Lancez la commande `npm run deploy` pour déployer le contrat localement
5.  Lancez la commande `npm run dev` pour lancer l'application web

## 📄 Documentation

Pour voir la documentation complète de la template utilisée: [create-web3.xyz](https://create-web3.xyz)
