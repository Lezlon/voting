//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.22;

import "@openzeppelin/contracts/access/Ownable.sol";

/**
 * @title Système de vote pour des élections
 * @author Nathan Hallez
 * @dev Le contrat se déroule en plusieurs étapes:
 * 1. À la création du contrat on précise les différents votants.
 * 2. Ces votants pourront ensuite faire des propositions lors de la session d'enregistrement
 * 3. Les votants sont ensuite soumis à un vote lors de la session de vote
 * 4. La proposition qui reçoit le plus de votes l'emporte
 */
contract Voting is Ownable {
    /**
     * @dev Représente un votant
     */
    struct Voter {
        uint weight; // weight is accumulated by delegation
        bool isRegistered;
        bool hasVoted;
        address delegate; // person delegated to
        uint256 votedProposalId;
    }

    /**
     * @dev Représente une proposition soumise par un votant
     */
    struct Proposal {
        string description;
        uint256 voteCount;
    }

    /**
     * @dev Énumération des différentes étapes du contrat
     */
    enum WorkflowStatus {
        RegisteringVoters,
        ProposalsRegistrationStarted,
        ProposalsRegistrationEnded,
        VotingSessionStarted,
        VotingSessionEnded,
        VotesTallied
    }

    event VoterRegistered(address voterAddress);
    event WorkflowStatusChange(
        WorkflowStatus previousStatus,
        WorkflowStatus newStatus
    );
    event ProposalRegistered(uint256 proposalId);
    event Voted(address voter, uint256 proposalId);

    WorkflowStatus private _currentStep;
    mapping(address => Voter) private _voters;
    /**
     * @dev On part du principe qu'il ne peut y avoir qu'une proposition par personne
     */
    mapping(address => bool) private _voterProposal;
    Proposal[] private _proposals;

    constructor() Ownable() {
        _currentStep = WorkflowStatus.RegisteringVoters;
    }

    /**
     * @dev La valeur n'est affectée que au moment des dépouillements des votes
     */
    uint256 private _winningProposalId;

    /**
     * @dev Vérifie que l'étape courante correspond bien à celle demandée
     */
    modifier atStep(WorkflowStatus requiredStatus) {
        require(
            _currentStep == requiredStatus,
            "Forbidden at current voting step"
        );
        _;
    }

    /**
     * @dev Modifier qui n'inclue que les votants ajoutés par l'administateur à l'étape 1
     */
    modifier onlyVoters() {
        require(_voters[msg.sender].isRegistered, "You have to be a voter!");
        _;
    }

    /**
     * @dev Fonction appelable uniquement à l'étape RegisteringVoters
     */
    function addVoter(
        address voter
    ) public atStep(WorkflowStatus.RegisteringVoters) onlyOwner {
        require(!_voters[voter].isRegistered, "User is already a voter!");
        _voters[voter].isRegistered = true;
        _voters[voter].weight = 1;
        emit VoterRegistered(voter);
    }

    /**
     * @dev Ne peut être lancée que après l'enregistrement des votes
     */
    function startProposalRegistration()
        public
        atStep(WorkflowStatus.RegisteringVoters)
        onlyOwner
    {
        _currentStep = WorkflowStatus.ProposalsRegistrationStarted;
        emit WorkflowStatusChange(
            WorkflowStatus.RegisteringVoters,
            WorkflowStatus.ProposalsRegistrationStarted
        );
    }

    modifier isUniqueProposal(string memory proposition) {
        for (uint i = 0; i < _proposals.length; i++) {
            require(
                keccak256(bytes(_proposals[i].description)) !=
                    keccak256(bytes(proposition)),
                "Proposal is not unique!"
            );
        }
        _;
    }

    /**
     * @dev Permet à un votant d'ajouter une proposition
     */
    function registerProposal(
        string memory proposition
    )
        public
        atStep(WorkflowStatus.ProposalsRegistrationStarted)
        onlyVoters
        isUniqueProposal(proposition)
    {
        require(
            !_voterProposal[msg.sender],
            "You have already added a proposal!"
        );
        _voterProposal[msg.sender] = true;
        _proposals.push(Proposal(proposition, 0));
        emit ProposalRegistered(_proposals.length - 1);
    }

    function endProposalRegistration()
        public
        atStep(WorkflowStatus.ProposalsRegistrationStarted)
        onlyOwner
    {
        require(
            _proposals.length > 0,
            "You need at least one proposal to end proposal registration!"
        );
        _currentStep = WorkflowStatus.ProposalsRegistrationEnded;
        emit WorkflowStatusChange(
            WorkflowStatus.ProposalsRegistrationStarted,
            WorkflowStatus.ProposalsRegistrationEnded
        );
    }

    function startVotingSession()
        public
        atStep(WorkflowStatus.ProposalsRegistrationEnded)
        onlyOwner
    {
        _currentStep = WorkflowStatus.VotingSessionStarted;
        emit WorkflowStatusChange(
            WorkflowStatus.ProposalsRegistrationStarted,
            WorkflowStatus.VotingSessionStarted
        );
    }

    /**
     * @dev Permet à un électeur inscrit de déléguer son vote à quelqu'un d'autre
     */
    function delegate(
        address to
    ) public atStep(WorkflowStatus.VotingSessionStarted) onlyVoters {
        Voter storage sender = _voters[msg.sender];
        require(!sender.hasVoted, "You already voted");
        require(to != msg.sender, "Self-delegation is disallowed");

        while (_voters[to].delegate != address(0)) {
            to = _voters[to].delegate;
            // Boucle infinie dans la délégation
            require(to != msg.sender, "Found loop in delegation");
        }
        sender.hasVoted = true;
        sender.delegate = to;
        Voter storage delegate_ = _voters[to];
        if (delegate_.hasVoted) {
            // Si l'utilisateur qui délègue le vote avait déjà voté on ajoute directement le nombre de votes
            _proposals[delegate_.votedProposalId].voteCount += sender.weight;
            emit Voted(msg.sender, delegate_.votedProposalId);
        } else {
            // Si l'utilisateur n'a pas encore voté, on ajoute le poids du vote
            delegate_.weight += sender.weight;
        }
    }

    /**
     * @dev Permet à un électeur inscrit de voter pour sa proposition préférée.
     */
    function vote(
        uint proposalId
    ) public atStep(WorkflowStatus.VotingSessionStarted) onlyVoters {
        require(!_voters[msg.sender].hasVoted, "You have already voted!");
        require(proposalId < _proposals.length, "Invalid Proposal ID");
        _voters[msg.sender].hasVoted = true;
        _voters[msg.sender].votedProposalId = proposalId;
        // Je ne vérifie pas les dépassements d'entiers, bonne chance pour arriver à ajouter 1*10^77 votes
        _proposals[proposalId].voteCount += _voters[msg.sender].weight;
        emit Voted(msg.sender, proposalId);
    }

    function endVotingSession()
        public
        atStep(WorkflowStatus.VotingSessionStarted)
        onlyOwner
    {
        _currentStep = WorkflowStatus.VotingSessionEnded;
        emit WorkflowStatusChange(
            WorkflowStatus.VotingSessionStarted,
            WorkflowStatus.VotingSessionEnded
        );
    }

    /**
     * @dev Dépouillement des votes
     */
    function tallyVotes()
        public
        atStep(WorkflowStatus.VotingSessionEnded)
        onlyOwner
    {
        uint winnerId = 0;
        for (uint i = 1; i < _proposals.length; i++) {
            if (_proposals[i].voteCount > _proposals[winnerId].voteCount) {
                winnerId = i;
            }
        }
        _winningProposalId = winnerId;
        _currentStep = WorkflowStatus.VotesTallied;
        emit WorkflowStatusChange(
            WorkflowStatus.VotingSessionEnded,
            WorkflowStatus.VotesTallied
        );
    }

    /**
     * VIEWS
     */

    function currentStep() public view virtual returns (WorkflowStatus) {
        return _currentStep;
    }

    function proposals() public view virtual returns (Proposal[] memory) {
        require(
            _currentStep != WorkflowStatus.RegisteringVoters,
            "Proposal registration has not started or ended!"
        );
        return _proposals;
    }

    function winner()
        public
        view
        virtual
        atStep(WorkflowStatus.VotesTallied)
        returns (Proposal memory)
    {
        return _proposals[_winningProposalId];
    }

    function getVoter() public view virtual returns (Voter memory) {
        return _voters[msg.sender];
    }

    function getNbVotes() public view virtual returns (uint256) {
        uint total = 0;
        for (uint i = 0; i < _proposals.length; i++) {
            total += _proposals[i].voteCount;
        }
        return total;
    }

    function hasMadeProposal() public view virtual returns (bool) {
        return _voterProposal[msg.sender];
    }

    function isOwner() public view virtual returns (bool) {
        return owner() == msg.sender;
    }
}
