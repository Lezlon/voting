export async function getAuthorizations(contract: any) {
    const voter = await contract.getVoter();
    const isOwner = await contract.isOwner();
    const hasMadeProposal = await contract.hasMadeProposal();
    return {
        isRegistered: voter.isRegistered,
        isOwner: isOwner,
        hasVoted: voter.hasVoted,
        votedProposalId: voter.votedProposalId,
        hasMadeProposal: hasMadeProposal,
        delegate: voter.delegate
    };
}