import { Context, FormEvent, createContext, useContext, useEffect, useState } from "react";
import { NETWORK_ID } from "./config";
import { useContract, useSigner } from "wagmi";
import contracts from '../contracts/hardhat_contracts.json';
import { Registration } from "./components/Registration";
import { Stepper } from "./components/Stepper";
import { AddProposal } from "./components/AddProposal";
import { Vote } from "./components/Vote";
import { Resultats } from "./components/Resultats";
import { ProposalEnded } from "./components/ProposalEnded";
import { getAuthorizations } from "./Authorizations";
import { VoteEnded } from "./components/VoteEnded";
import { Signer, ethers } from "ethers";

export let contract: any;
export let authorizations: any;
let setAuthorizations: any;

export function updateAuthorizations(contract: any) {
    getAuthorizations(contract).then((res) => setAuthorizations(res));
}

export function EthContainer() {
    const chainId = Number(NETWORK_ID);
    const allContracts = contracts as any;
    const greeterAddress = allContracts[chainId][0].contracts.Voting.address;
    const greeterABI = allContracts[chainId][0].contracts.Voting.abi;
    const { data: signerData } = useSigner();
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(''); 

    contract = new ethers.Contract(greeterAddress, greeterABI, signerData as Signer);

    let [step, setStep] = useState(0);

    [authorizations, setAuthorizations] = useState({
        isRegistered: false,
        isOwner: false,
        hasVoted: false,
        votedProposalId: 0
    });    

    useEffect(() => {
        if (signerData) {
            setError('');
            contract?.currentStep().then((change: any) => {
                setStep(change);
                setLoading(false);
            });
            contract?.on("WorkflowStatusChange", async () => {
                const change = await contract.currentStep();
                setStep(change);
                // Si on est à l'étape 1, l'utilisateur peut-être ajouté dans la liste des votants
                if (change == 1) {
                    updateAuthorizations(contract);
                }
            });   
        } else {
            setLoading(false);
            setError('Veuillez connecter votre wallet');
        } 

        if (contract?.provider != null) {
            getAuthorizations(contract).then((res) => setAuthorizations(res));
        }
    }, [signerData]);

    if (loading) {
        return <div className="w-full text-center">Loading...</div>;
    }

    if (error) {
        return <div className="w-full text-center">{error}</div>;
    }

    return (
        <div>
            <Stepper currentStep={step} />
            <div className="min-h-[60vh] flex flex-1 flex-col justify-center items-center mt-10 px-5">
                <div className="w-full max-w-3xl ">
                    {step == 0 && (<Registration/>)}
                    {step == 1 && (<AddProposal/>)}
                    {step == 2 && (<ProposalEnded/>)}
                    {step == 3 && (<Vote/>)}
                    {step == 4 && (<VoteEnded/>)}
                    {step == 5 && (<Resultats/>)}
                </div>
            </div>
        </div>
    );
}