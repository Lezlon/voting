import { useContext, useEffect, useState } from "react";
import { contract } from "../EthContainer";

export function Resultats() {
    let [winner, setWinner] = useState({
        description: '',
        voteCount: 0
    });

    async function getWinner() {
        const response = (await contract.winner());
        setWinner(response);
    }
    
    useEffect(() => {
        getWinner();
    }, []);

    return (
        <div>
            <h1>Résultats</h1>
            <p className="mb-2"><strong>Nombre de votes:</strong> {winner?.voteCount.toString()}</p>
            <p className="mb-2"><strong>Proposition gagnante:</strong></p>
            <p className="bg-white p-3 rounded-lg drop-shadow code-font">{winner?.description}</p>
        </div>
    );
}