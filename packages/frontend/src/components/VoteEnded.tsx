import { useContext } from "react";
import { authorizations, contract } from "../EthContainer";
import { InfoText } from "./InfoText";

export function VoteEnded() {
    const nextStep = async () => {
        const tx = await contract.tallyVotes();
        await tx.wait();
    }

    return (
        <>
        <h1>Session de vote</h1>
        <InfoText description="La phase de vote est terminée!"/>
        { !authorizations.isOwner && <p>En attente de l'administrateur...</p> }
        { authorizations.isOwner && <button className="btn-secondary" onClick={nextStep}>Étape suivante</button> }
        </>
    );
}