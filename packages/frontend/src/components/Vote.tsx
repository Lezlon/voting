import { FormEvent, useContext, useEffect, useState } from "react";
import { authorizations, contract, updateAuthorizations } from "../EthContainer";
import { SubmitHandler, useForm } from "react-hook-form";
import { getErrorFromContract } from "./ErrorHandler";
import { InfoText } from "./InfoText";

export function Vote() {
    const nextStep = async () => {
        const tx = await contract.endVotingSession();
        await tx.wait();
    }

    const [proposals, setProposals] = useState<string[]>([]);
    const [nbVoters, setNbVoters] = useState<number>(0);
    const [nbVotes, setNbVotes] = useState<number>(0);

    const getCurrentProposals = async () => {
        let events = (await contract.proposals()).map((event: any) => {
            return event.description;
        });
        setProposals(events);
    }

    const getNbVoters = async () => {
        setNbVoters((await contract.queryFilter("VoterRegistered")).length);
    }

    useEffect(() => {
        getCurrentProposals();
        getNbVoters();
        updateNbVotes();
        contract.on("Voted", updateNbVotes);
    }, []);

    async function vote(e: FormEvent, proposal: number) {
        e.preventDefault();
        const tx = await contract.vote(proposal);
        await tx.wait();
        updateAuthorizations(contract);
    }


    const updateNbVotes = async () => {
        const txResult = await contract.callStatic.getNbVotes();
        setNbVotes(txResult.toString());
    }

    type Inputs = {
        address: string
    }

    const {
        register,
        handleSubmit,
        setError,
        formState: { errors },
    } = useForm<Inputs>()
    
    const onSubmit: SubmitHandler<Inputs> = async (data) => {
        try {
            const tx = await contract?.delegate(data.address);
            await tx.wait();
            updateAuthorizations(contract);
        } catch (e: any) {
            setError('address', getErrorFromContract(e));
        }
    }

    return (
        <div>
            <h1>Session de vote</h1>
            <ul className="flex flex-col gap-3 mb-3 overflow-y-auto max-h-96 pb-3">
                {proposals.map((proposal: any, index: number) => (
                    <li key={proposal} className="flex gap-2 items-center justify-between bg-white p-3 rounded-lg drop-shadow code-font">
                        <p className="ml-1 w-fit text-ellipsis overflow-hidden">{proposal}</p>
                        { authorizations.isRegistered && !authorizations.hasVoted && 
                        <button className="btn-primary-no-margin ml-3 w-auto" onClick={(e) => vote(e, index)}>Voter</button> }
                        { authorizations.hasVoted && authorizations.delegate == 0 && authorizations.votedProposalId == index &&
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="white" className="block max-w-[1.5rem] bg-green-400 rounded-full p-1">
                            <path strokeLinecap="round" strokeLinejoin="round" d="M4.5 12.75l6 6 9-13.5" />
                        </svg>}
                    </li>
                ))}
            </ul>

            { authorizations.isRegistered && !authorizations.hasVoted &&
            <form onSubmit={handleSubmit(onSubmit)} className="w-full mt-5">
                <label htmlFor="address" className="label-input">Délégation</label>
                <div className="flex gap-1">
                    <div className="mb-5 grow">
                        <input id="address" placeholder="0x9965507D1a55bcC2695C58ba16FB37d819B0A4dc" className="text-input"
                        {...register("address",
                        {
                            required: "Champ requis",
                            pattern: {
                                value: /^0x[a-fA-F0-9]{40}$/,
                                message: "Veuillez saisir une adresse Ethereum valide"
                            }
                        })}
                        />
                        <p>{errors.address?.message}</p>
                    </div>
                    <button className="btn-primary-no-margin h-fit" color="blue" type="submit">Déléguer mon vote</button>
                </div>
            </form>}

            { authorizations.delegate > 0 && <InfoText description="Vous avez délégué votre vote" /> }

            <p>{nbVotes}/{nbVoters} votes enregistrés</p>
            { authorizations.isOwner && <button onClick={nextStep} className="btn-secondary" disabled={nbVotes==0}>Étape suivante</button>}
        </div>
    );
}