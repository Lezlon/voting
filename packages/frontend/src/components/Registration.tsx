import { createRef, useContext, useEffect, useMemo, useRef, useState } from "react"
import { authorizations, contract } from "../EthContainer"
import { createAvatar } from "@dicebear/core";
import { identicon } from '@dicebear/collection';
import { useForm, SubmitHandler } from "react-hook-form"
import { getErrorFromContract } from "./ErrorHandler";
import { InfoText } from "./InfoText";

export function Registration() {
    const eventFilter = contract.filters.VoterRegistered();

    const [voters, setVoters] = useState<string[]>([]);

    const getCurrentVoters = async () => {
        let events = (await contract.queryFilter(eventFilter)).map((event: any) => {
            return '0x' + event.data.match(/0x0+(.*)/)[1]
        });
        events.sort((a: string, b: string) => a.localeCompare(b));
        setVoters(events);
    }

    useEffect(() => {
        getCurrentVoters();
        contract.on(eventFilter, getCurrentVoters);
    }, []);

    const generateAvatar = (address: string) => {
        return createAvatar(identicon, {
            size: 32,
            seed: address
        }).toDataUriSync();
    }

    const nextStep = async () => {
        const tx = await contract.startProposalRegistration();
        await tx.wait();
    }

    type Inputs = {
        address: string
    }

    const {
        register,
        handleSubmit,
        setError,
        formState: { errors },
    } = useForm<Inputs>()
    
    const onSubmit: SubmitHandler<Inputs> = async (data) => {
        try {
            const tx = await contract?.addVoter(data.address);
            await tx.wait();
        } catch (e: any) {
            setError('address', getErrorFromContract(e));
        }
    }

    return (
        <div>
            <h1>Ajout des électeurs</h1>
            <label className="label-input">Électeurs</label>
            {voters.length == 0 && <InfoText description="Aucun électeur ajouté pour le moment"/>}
            <div className="flex flex-col gap-3 overflow-y-auto max-h-96 pb-3">
                {voters.map(voter => (
                    <div key={voter} className="flex gap-2 items-center bg-white p-3 rounded-lg drop-shadow">
                        <img src={generateAvatar(voter)} alt="avatar"/>
                        <p className="code-font text-ellipsis overflow-hidden w-fit">{voter}</p>
                    </div>
                ))}
            </div>
            { authorizations?.isOwner &&
            <form onSubmit={handleSubmit(onSubmit)} className="w-full mt-5">
                <label htmlFor="address" className="label-input">Adresse Ethereum</label>
                <div className="mb-5">
                    <input id="address" placeholder="0x9965507D1a55bcC2695C58ba16FB37d819B0A4dc" className="text-input"
                    {...register("address",
                    {
                        required: "Champ requis",
                        pattern: {
                            value: /^0x[a-fA-F0-9]{40}$/,
                            message: "Veuillez saisir une adresse Ethereum valide"
                        }
                    })}
                    />
                    <p>{errors.address?.message}</p>
                </div>
                <button className="btn-primary w-full" color="blue" type="submit">Ajouter un électeur</button>
            </form>}
            { authorizations?.isOwner && <button className="btn-secondary w-full" onClick={nextStep} disabled={!voters.length}>Étape suivante</button> }
        </div>
    );
}