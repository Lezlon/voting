import { useContext } from "react";
import { authorizations, contract } from "../EthContainer";
import { InfoText } from "./InfoText";

export function ProposalEnded() {
    const nextStep = async () => {
        const tx = await contract.startVotingSession();
        await tx.wait();
    }

    return (
        <>
        <h1>Ajout des propositions</h1>
        <InfoText description="La phase de propositions est terminée!"/>
        { !authorizations.isOwner && <p>En attente de l'administrateur...</p> }
        { authorizations.isOwner && <button className="btn-secondary" onClick={nextStep}>Étape suivante</button> }
        </>
    );
}