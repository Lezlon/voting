export function getErrorFromContract(error: any) {
    let errorMessage;
    if (error.reason) {
        const customError = error.reason.match(/Error: VM Exception while processing transaction: reverted with reason string '(.*)'/);
        const defaultError = error.reason;
        errorMessage = customError ? customError[1] : defaultError;
    } else {
        errorMessage = "Erreur dans la transaction, essayez de réinitialiser votre wallet"        
    }
    return { type: 'custom', message: errorMessage };
}