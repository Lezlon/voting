import { FormEvent, useContext, useEffect, useState } from "react";
import { authorizations, contract, updateAuthorizations } from "../EthContainer";
import { SubmitHandler, useForm } from "react-hook-form";
import { getErrorFromContract } from "./ErrorHandler";
import { InfoText } from "./InfoText";

export function AddProposal() {
    const [proposals, setProposals] = useState<string[]>([]);

    const getCurrentProposals = async () => {
        let events = (await contract.proposals()).map((event: any) => {
            return event.description;
        });
        setProposals(events);
        updateAuthorizations(contract);
    }

    useEffect(() => {
        getCurrentProposals();
        contract.on("ProposalRegistered", getCurrentProposals);
    }, []);

    const nextStep = async () => {
        const tx = await contract.endProposalRegistration();
        await tx.wait();
    }

    type Inputs = {
        proposal: string
    }

    const {
        register,
        handleSubmit,
        setError,
        formState: { errors },
    } = useForm<Inputs>()
    
    const onSubmit: SubmitHandler<Inputs> = async (data) => {
        try {
            const tx = await contract.registerProposal(data.proposal);
            await tx.wait();
        } catch (e: any) {
            setError('proposal', getErrorFromContract(e));
        }
    }

    return (
        <div>
            <h1>Ajout des propositions</h1>
            <ul className="flex flex-col gap-3 overflow-y-auto max-h-96 pb-3 w-full">
                {proposals.map(proposal => (
                    <li key={proposal} className="flex gap-2 items-center bg-white p-3 rounded-lg drop-shadow code-font">
                        <p className="text-ellipsis overflow-hidden">{proposal}</p>
                    </li>
                ))}
            </ul>
            { authorizations.isRegistered && !authorizations.hasMadeProposal &&
            <form onSubmit={handleSubmit(onSubmit)} className="mt-5 w-full">
                <label htmlFor="proposal" className="Proposition">Proposition</label>
                <div className="mb-5">
                    <textarea className="text-input" id="proposal" placeholder="Saisissez votre proposition..."
                    {...register("proposal",
                    {
                        required: "Champ requis",
                    })}
                    ></textarea>
                    <p>{errors.proposal?.message}</p>
                </div>
                <button className="btn-primary w-full" color="blue" type="submit">Ajouter une proposition</button>
            </form> }
            
            { !authorizations.isRegistered && <InfoText description="Vous ne pouvez pas ajouter de propositions"/> }
            { authorizations.hasMadeProposal && <InfoText description="Vous avez déjà ajouté une proposition"/> }
            { authorizations.isOwner && <div className="w-full"><button className="btn-secondary" onClick={nextStep} disabled={!proposals.length}>Étape suivante</button></div> }
        </div>
    );
}