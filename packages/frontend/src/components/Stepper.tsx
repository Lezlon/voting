export function Stepper({currentStep}: any) {
    const steps = new Map<number, string>([
        [0, "Ajout des électeurs"],
        [1, "Ajout des propositions"],
        [3, "Session de vote"],
        [5, "Résultats"]
    ]);
    if (currentStep == 2) currentStep = 1;
    if (currentStep == 4) currentStep = 3;
    return (
    <div className="relative after:absolute after:inset-x-0 after:top-1/2 after:block after:h-0.5 after:-translate-y-1/2 after:rounded-lg after:bg-gray-100 mx-5">
        <ol className="relative z-10 flex justify-between text-sm font-medium text-gray-500">
            {Array.from(steps.entries()).map((entry: any, index: number) => (
            <li className="flex items-center gap-2 bg-white p-2" key={index}>
                {entry[0] != currentStep &&
                <span className="h-6 w-6 rounded-full bg-gray-100 text-center text-[10px]/6 font-bold">
                    {index + 1}
                </span>}
                {entry[0] == currentStep &&
                <span className="h-6 w-6 rounded-full bg-blue-700 text-center text-[10px]/6 font-bold text-white">
                    {index + 1}
                </span>}
                <span className="hidden sm:block">{entry[1]}</span>
            </li>))}
        </ol>
    </div>
    )
}