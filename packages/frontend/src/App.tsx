import { ConnectButton } from '@rainbow-me/rainbowkit';
import { EthContainer } from './EthContainer';

function App() {
  return (
    <div className="">
      <header style={{ padding: '1rem' }}>
        <ConnectButton />
      </header>
      <main>
        <EthContainer />
      </main>
    </div>
  );
}

export default App;
